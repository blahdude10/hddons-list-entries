#!/usr/bin/env bash

regex=${1}
new=${2}

if [[ ${regex} == "" || ${new} == "" ]]
then
    echo "Usage: ${0} <regex> <replacement>"
    echo "Description: Replaces any regex matches with the provided replacement text for all entries."
    exit
fi

for i in *
do
    if [[ -d ${i} && ${i} != "list_output" ]]
    then
        for f in ${i}/*
        do
            if [[ "${OSTYPE}" == "darwin"* ]]
            then
                sed -i "" -e "s/${regex}/${new}/g" "${f}"
            else
                sed -i "s/${regex}/${new}/g" "${f}"
            fi
        done
    fi
done
